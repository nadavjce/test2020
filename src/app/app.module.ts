import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';

import {MatExpansionModule} from '@angular/material/expansion';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './auth.service';



import { RouterModule, Routes } from '@angular/router';
import { DeleteComponent } from './delete/delete.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NavComponent } from './nav/nav.component';
import { SignupsuccesComponent } from './signupsucces/signupsucces.component';
import { MainComponent } from './main/main.component';

const appRoutes: Routes=[
  { path: 'signup', component: SignupComponent },
  // { path: 'article-form', component: ArticleFormComponent },
  // { path: 'article_edit', component: ArticleEditComponent },
  // { path: 'article_edit/:key_id', component: ArticleEditComponent },

  // { path: 'classified-article', component: ClassifiedArticleComponent },
   { path: 'signupsucces', component: SignupsuccesComponent },
   { path: 'login', component: LoginComponent },
   { path: 'main', component: MainComponent },
  // { path: 'classtified_collection', component: ClasstifiedCollectionComponent },
  {path:"", redirectTo:'/login', pathMatch:'full'},
];









@NgModule({
  declarations: [
    AppComponent,
    DeleteComponent,
    LoginComponent,
    SignupComponent,
    NavComponent,
    SignupsuccesComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule, 
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule
    .forRoot(
      appRoutes,
    //  {enableTracing:true}    // this is for debugging only
    )

  ],
  providers: [AngularFireAuth,
    AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
